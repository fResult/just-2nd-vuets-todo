export type Todo = {
  id: number
  description: string
  completed: boolean
}

export type FilteredMode = 'all' | 'active' | 'completed'
